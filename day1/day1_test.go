package main

import (
	"testing"
	//"gitlab.com/drakonka/adventofcode2017/util"
	"os"
	"gitlab.com/drakonka/adventofcode2017/util"
)

func TestMain(m *testing.M) {

	ret := m.Run()
	if ret == 0 {
		util.CreateReportHeaders(1)
		s1res := testing.Benchmark(BenchmarkD1P1Slice)
		util.RecordBenchRes(1, 1, "Slice", s1res)
		ll1res := testing.Benchmark(BenchmarkD1P1LinkedList)
		util.RecordBenchRes(1, 1, "LinkedList", ll1res)
		s2res := testing.Benchmark(BenchmarkD1P2Slice)
		util.RecordBenchRes(1, 2, "Slice", s2res)
		ll2res := testing.Benchmark(BenchmarkD1P2LinkedList)
		util.RecordBenchRes(1, 2, "LinkedList", ll2res)
	}
	os.Exit(ret)
}

func TestD1P1SliceAnswer(t *testing.T) {
	want := 1177
	a := runPartOneSlice()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD1P1Slice(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartOneSlice()
	}
}

func TestD1P1LinkedListAnswer(t *testing.T) {
	want := 1177
	a := runPartOneLL()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD1P1LinkedList(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartOneLL()
	}
}

func TestD1P2SliceAnswer(t *testing.T) {
	want := 1060
	a := runPartTwoSlice()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD1P2Slice(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartTwoSlice()
	}
}

func TestD1P2LLAnswer(t *testing.T) {
	want := 1060
	a := runPartTwoLL()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}


func BenchmarkD1P2LinkedList(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartTwoLL()
	}
}