package main

import (
	"strconv"
)

func runPartOneSlice() int {
	var sum int
	firstEl, _ := strconv.Atoi(string(input[0]))
	lastEl := firstEl
	for i := 1; i < len(input); i++ {
		el, _ := strconv.Atoi(string(input[i]))
		if el == lastEl {
			sum += el
		}
		lastEl = el
	}
	if firstEl == lastEl {
		sum += firstEl
	}
	return sum
}

func runPartTwoSlice() int {
	var sum int
	inputLen := len(input)
	steps := inputLen / 2
	for i := 0; i < inputLen; i++ {
		el, _ := strconv.Atoi(string(input[i]))
		var idx int
		if i >= inputLen / 2 {
			idx = steps - (inputLen - i)
		} else {
			idx = i + steps
		}
		tocompare, _ := strconv.Atoi(string(input[idx]))
		if el == tocompare {
			sum += el
		}
	}
	return sum
}

