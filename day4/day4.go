package main

import (
	"os"
	"fmt"
	"runtime"
	"bufio"
	"strings"
	"sort"
	"gitlab.com/drakonka/adventofcode2017/util"
)

func main() {
	fmt.Println("----------------------------------------")
	fmt.Println("DAY 4")
	fmt.Println("----------------------------------------")

	fmt.Printf("\nPart 1 res: %d", runPartOne())
	fmt.Printf("\nPart 2 res: %d", runPartTwo())


	fmt.Println()
	fmt.Println("----------------------------------------")
	fmt.Println("END DAY 4")
	fmt.Println("----------------------------------------")

}

func runPartOne() int {
	inputFile := getInputFile()
	defer inputFile.Close()

	var validcount int
	scanner := bufio.NewScanner(inputFile)

	for scanner.Scan() {
		isValid := true
		line := scanner.Text()
		words := strings.Split(line, " ")
		TopFor:
			for len(words) > 0 {
				word := words[0]
				for i2 := 1; i2 < len(words); i2++ {
					otherword := words[i2]
					if word == otherword {
						isValid = false
						break TopFor
					}
				}
				words = append(words[:0], words[0+1:]...)
			}
		if isValid {
			validcount++
		}
	}
	return validcount
}

func runPartTwo() int {
	inputFile := getInputFile()
	defer inputFile.Close()

	var validcount int
	scanner := bufio.NewScanner(inputFile)

	for scanner.Scan() {
		isValid := true
		line := scanner.Text()
		words := strings.Split(line, " ")
	TopFor:
		for len(words) > 0 {
			word := words[0]
			for i2 := 1; i2 < len(words); i2++ {
				otherword := words[i2]
				if isAnagram(word, otherword){
					isValid = false
					break TopFor
				}
			}
			words = append(words[:0], words[0+1:]...)
		}
		if isValid {
			validcount++
		}
	}
	return validcount
}

func getInputFile() *os.File {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil
	}
	return util.GetInputFile(filename)
}

func isAnagram(w1, w2 string) bool {
	if len(w1) != len(w2) {
		return false
	}
	w1s := strings.Split(w1, "")
	sort.Strings(w1s)

	w2s := strings.Split(w2, "")
	sort.Strings(w2s)

	sortedw1 := strings.Join(w1s,"")
	sortedw2 := strings.Join(w2s,"")

	if sortedw1 == sortedw2 {
		return true
	}
	return false
}