package main

import (
	"testing"
	"os"
	"gitlab.com/drakonka/adventofcode2017/util"
)

func TestMain(m *testing.M) {
	ret := m.Run()
	if ret == 0 {
		util.CreateReportHeaders(2)
		p1res := testing.Benchmark(BenchmarkD2P1)
		util.RecordBenchRes(2, 1, "", p1res)
		p2res := testing.Benchmark(BenchmarkD2P2)
		util.RecordBenchRes(2, 2, "", p2res)
	}
	os.Exit(ret)
}

func TestD2P1Answer(t *testing.T) {
	want := 42299
	a := runPartOne()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD2P1(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartOne()
	}
}

func TestD2P2Answer(t *testing.T) {
	want := 277
	a := runPartTwo()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD2P2(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartTwo()
	}
}