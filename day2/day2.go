package main

import (
	"os"
	"fmt"
	"bufio"
	"strings"
	"runtime"
	"strconv"
	"gitlab.com/drakonka/adventofcode2017/util"
)

/*
	PART ONE:

	The spreadsheet consists of rows of apparently-random numbers.
	To make sure the recovery process is on the right track, they need you to calculate the spreadsheet's checksum.
	For each row, determine the difference between the largest value and the smallest value;
	the checksum is the sum of all of these differences.

	---
	PART TWO:

	It sounds like the goal is to find the only two numbers in each row where one evenly divides the other -
	that is, where the result of the division operation is a whole number.
	They would like you to find those numbers on each line, divide them, and add up each line's result.

 */

func main() {
	fmt.Println("----------------------------------------")
	fmt.Println("DAY 2")
	fmt.Println("----------------------------------------")

	fmt.Printf("\nPart 1 res: %d", runPartOne())
	fmt.Printf("\nPart 2 res: %d", runPartTwo())

	fmt.Println()
	fmt.Println("----------------------------------------")
	fmt.Println("END DAY 2")
	fmt.Println("----------------------------------------")

}

func getInputFile() *os.File {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil
	}
	return util.GetInputFile(filename)
}

func runPartOne() int {
	inputFile := getInputFile()
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)

	var sum int
	for scanner.Scan() {
		line := scanner.Text()
		nums := strings.Split(line, "\t")
		lowest, _ := strconv.Atoi(nums[0])
		highest := lowest
		for i := 1; i < len(nums); i++ {
			num, _ := strconv.Atoi(nums[i])
			if num < lowest {
				lowest = num
			} else if num > highest {
				highest = num
			}
		}
		sum += highest - lowest
	}
	inputFile.Seek(0,0)
	return sum
}

func runPartTwo() int {
	inputFile := getInputFile()
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	var sum int
	for scanner.Scan() {
		line := scanner.Text()
		nums := strings.Split(line, "\t")
		SearchLoop:
			for i := 0; i < len(nums); i++ {
				num, _ := strconv.Atoi(nums[i])
				for n := 0; n < len(nums); n++ {
					if n == i {
						continue
					}
					num2, _ := strconv.Atoi(nums[n])
					if num % num2 == 0  {
						sum += num / num2
						break SearchLoop
					} else if num2 % num == 0 {
						sum += num2 / num
						break SearchLoop
					}
				}
			}
	}
	inputFile.Seek(0,0)
	return sum
}