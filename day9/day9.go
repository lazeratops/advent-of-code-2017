package main

import (
	"fmt"
	"runtime"
	"io/ioutil"
	"path"
)

func main() {
	fmt.Println("----------------------------------------")
	fmt.Println("DAY 9")
	fmt.Println("----------------------------------------")

	p1, p2 := runPuzzle()
	fmt.Printf("\nPart one: %d; Part two: %d", p1, p2)

	fmt.Println()
	fmt.Println("----------------------------------------")
	fmt.Println("END DAY 9")
	fmt.Println("----------------------------------------")
}

func runPuzzle() (int, int) {
	_, filename, _, _ := runtime.Caller(0)
	filepath := path.Join(path.Dir(filename), "input.txt")
	stream, err := ioutil.ReadFile(filepath)
	if err != nil {
		fmt.Printf("\n Something bad happened :( %s", err)
	}
	var score, nestingLevel, garbageCount int
	var isInGarbage bool
	for i := 0; i < len(stream); i++ {
		b := stream[i]
		if b == '!' {
			i++
			continue
		}
		if !isInGarbage  {
			if  b == '{' {
				nestingLevel++
				score += nestingLevel
			} else if b == '<' {
				isInGarbage = true
			} else if b == '}' {
				nestingLevel--
			}
		} else {
			if b == '>' {
				isInGarbage = false
			} else {
				garbageCount++
			}
		}
	}
	return score, garbageCount
}