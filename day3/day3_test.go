package main

import (
	"testing"
	"os"
	"gitlab.com/drakonka/adventofcode2017/util"
)

func TestMain(m *testing.M) {
	ret := m.Run()
	if ret == 0 {
		util.CreateReportHeaders(3)
		p1res := testing.Benchmark(BenchmarkD3P1)
		util.RecordBenchRes(3, 1, "", p1res)
		p2res := testing.Benchmark(BenchmarkD3P2)
		util.RecordBenchRes(3, 2, "", p2res)
	}
	os.Exit(ret)
}

func TestD3P1Answer(t *testing.T) {
	want := 552
	a := runPartOne()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD3P1(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartOne()
	}
}

func TestD3P2Answer(t *testing.T) {
	want := 330785
	a := runPartTwo()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD3P2(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartTwo()
	}
}